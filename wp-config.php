<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4Hn+b0W7hfKlsyflAWtQQFNkxK7SYzgXI34w7IBADHNGcl9YWvJzxFu+zq3ghtZwOlpir6x40diLKibWrCawuw==');
define('SECURE_AUTH_KEY',  'RuU5XJ4q1r5SesDKioejUwdTLsPH64hkaPY9Ax1bH6b8bPcwk9fKNyfm3mp+ULuPCg8+8eN3dnInVg1A8IkgWA==');
define('LOGGED_IN_KEY',    'FGfHH6WWwn239XO2q7nfTB5PN9kQNzS4TYfJI69jSFgp83ugFFvrjgf8D3Qf08HBYXNomEYQ1DFXxvIykRdfmA==');
define('NONCE_KEY',        'yJHWSIbCAQvXQ4TCPCIB6N1MnZMrAsgF+KrHZVAk8WSZyyBVUQWGWFkZZRxB8PJlm9IYOh9BnEYaIIdrJxk6bg==');
define('AUTH_SALT',        '+LsnqOhoTka5nm645x5oMKRrbCPIGfmWTXAavwvsD1e4IUwjbzeNtamNaZQ9EhKU3wfSGLhZoBNpJ06RmReMVQ==');
define('SECURE_AUTH_SALT', 'ykKBixDDtmLmPcZtGxvqvc3RPqq9v5j68r+yw6ra8JwzdbVAVEdYVTd/fUnDqSFeOT+Bvm7eC5g/z0IX+9GiZw==');
define('LOGGED_IN_SALT',   'izO31C8LwoDShttZ3vJskPVczKT+qGxgRfgVl5yQ703t2p2X3L9fqfs3DiE/PunkRKFw3xRlg0qHpWnkBIjCHA==');
define('NONCE_SALT',       'e+jDpBBVAjh8kE7Sp5IdZn2vdEf/V803p9Fgu2K49VDKtWsyXScZHRuhKMENLD0krv6rL65N/OSO1BKWFjneKg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
